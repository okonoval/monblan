$(document).ready(function () {
    // Init switchClass for Events-blocks
    switchClass();
    moreEvents();
});

// switchClass for Events-blocks
function switchClass() {
    var switcherButtons = $('.switcher button');
    switcherButtons.on('click', function (e) {
        switcherButtons.removeClass('active');
        $(this).addClass('active')

        if ($(this).hasClass('grid')) {
            $('.events-block').removeClass('list').addClass('grid');
        } else if ($(this).hasClass('list')) {
            $('.events-block').removeClass('grid').addClass('list');
        }
    });
}

$(window).load(function () {
    moreEvents();
});

$(function () {
    var dateFormat = "mm/dd/yy",
        from = $("#from")
            .datepicker({
                defaultDate: "+1w",
                changeMonth: false,
                showOn: "both",
                numberOfMonths: 1,
                buttonImage: "images/icon1.png"
            })
            .on("change", function () {
                to.datepicker("option", "minDate", getDate(this));
            }),
        to = $("#to").datepicker({
            defaultDate: "+1w",
            changeMonth: false,
            numberOfMonths: 1,
            showOn: "both",
            buttonImage: "images/icon1.png"
        })
            .on("change", function () {
                from.datepicker("option", "maxDate", getDate(this));
            });

    function getDate(element) {
        var date;
        try {
            date = $.datepicker.parseDate(dateFormat, element.value);
        } catch (error) {
            date = null;
        }

        return date;
    }

    var $dates = $('#from').datepicker();

    $('#clear-dates').on('click', function () {
        $dates.datepicker('setDate', null);
    });
    var $dates2 = $(' #to').datepicker();

    $('#clear-dates2').on('click', function () {
        $dates2.datepicker('setDate', null);
    });
});

function moreEvents() {
    size_li = $(".events-block .event").size();
    var INIT_PRODUCTS_AMOUNT = 9;
    var PRODUCTS_PER_LOAD = 3;
    var currentlyVisibleProducts = INIT_PRODUCTS_AMOUNT;
    //--- first load ---//
    $('.events-block .event:lt(' + INIT_PRODUCTS_AMOUNT + ')').css('display', 'flex');

    //--- click on 'load more' ---//
    $('.btn-holder-more .moreEvent').click(function () {
        var nextCurrentlyVisivleProducts = currentlyVisibleProducts + PRODUCTS_PER_LOAD;

        if (nextCurrentlyVisivleProducts <= size_li) {
            currentlyVisibleProducts = nextCurrentlyVisivleProducts;
        } else {
            currentlyVisibleProducts = size_li;
        }
        $('.events-block .event:lt(' + currentlyVisibleProducts + ')').fadeIn().css('display', 'flex');

        //--- can we hide 'more'  button---//
        if (currentlyVisibleProducts == size_li) {
            $('.btn-holder-more .moreEvent').hide();
        }

        return false;
    });
    if (size_li > 12) {
        $('.btn-holder-more .moreEvent').css("display", "inline-block");
    }
}



